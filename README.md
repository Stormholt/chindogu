

# Getting started:
Assuming you have node-red locally installed

In a terminal run:
```bash
# assuming 
cd ~/.node-red/uibuilder
git clone https://gitlab.com/Stormholt/chindogu.git
cd chindogu
npm install
npm run build
```
It has been built under `~/.node-red/uibuilder/chindogu/dist` (see `.env`). 

You may need to configure uibuilder to build based on `~/.node-red/uibuilder/chindogu/dist` 

Run node-red, add a uibuilde component to the flow, make the following configurations:
1. change URL field from uibuilder to chindogu 
2. Advanced Settings >> Template >> change from src to dist
3. Manage front end libraries >> + add >> install : react, reactjs, react-router-dom, react-dom, react-bootstrap, react-simple-captcha
4. deploy

You should now be able to open http://localhost:1880/chindogu

If you make any change to the project you must run:
```bash
npm run build 
```
# Using the uibuilder component in flows
The uibuilder has 1 input and 2 outputs, this application only uses the upper output.

# development server
```bash
npm run start
```
Opens http://localhost:3000/chindogu

Note this works because `package.json:proxy` resolves to the node-red instance (by default http://localhost:1880).


# TODO
* Cannot have two and the same username
* Lock reservation
* Lock and unlock animation
* Timelock