import { useState, useEffect, useCallback} from "react";
import { loadCaptchaEnginge, LoadCanvasTemplateNoReload, validateCaptcha } from 'react-simple-captcha';
import './scenes/UserData'
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import uibuilder from 'node-red-contrib-uibuilder/front-end/src/uibuilderfe'
import {useNavigate} from 'react-router-dom'
import openImg from './PngItem_1870837_open.png'
import lockedImg from './PngItem_1870837_open.png'

function Dashboard(props){
    uibuilder.start()
    const [lock, setLock] = useState(false);
    const [user_captcha_value,setUserCaptcha] =useState("");
    const [captchaDone, setCaptchaDone] = useState(false);
    const [captchaError, setCaptchaError] = useState("");
    const [email, setEmail] = useState("");
    const [emailError, setEmailError] = useState("");
    const [emailSubmitted, setEmailSubmitted] = useState(false);
    const [code, setCode] = useState("");
    const [userCode, setUserCode] = useState("");
    const [noderedToggle, setNoderedToggle] = useState(false);
    const numberOfTries = 1;
    const [beltStatus, setBeltStatus] = useState("");
    const [nodeMsgRecv, setNodeMsgRecv] = useState(undefined);
    const [nodeMsgSent, setNodeMsgSent] = useState(undefined);
    const [socket, setSocket]= useState(undefined);
    const [codeError, setCodeError] = useState("");
    const [codeTries, setCodeTries] = useState(0);
    const navigate = useNavigate();
 //   const navigateLogin  = useCallback(() => navigate('/login', {replace: true}), [navigate]);

    useEffect(() => {
        if(captchaDone===false){
              loadCaptchaEnginge(6);
        }
      
    },[]);

    useEffect(() => {
        if(nodeMsgRecv !==undefined){
            setBeltStatus(nodeMsgRecv['payload']);
        
            if (nodeMsgRecv['payload'].toString().includes("open")){
                document.getElementById('header').style.backgroundImage = "url(./static/media/PngItem_1870837_open.24963a0bce4eb472a263.png)";
            }
            if (nodeMsgRecv['payload'].toString().includes("closed")){
                document.getElementById('header').style.backgroundImage = "url(./static/media/PngItem_1870837.cd8f57da5093ee98c190.png)";
            }
        }
    },[nodeMsgRecv]);

    useEffect(() => {
        if (lock === false){
            setEmailSubmitted(false);
        }
    },[lock]);

    uibuilder.onChange('sentMsg', function(newVal) {
        console.info('[uibuilder.onChange:sentMsg] msg sent to Node-RED server:', newVal);

        setNodeMsgSent(newVal);
    })

    const interval = setInterval(function() {
        uibuilder.onChange('ioConnected', (newVal) => {
            console.info('[uibuilder.onChange:ioConnected] Socket.IO Connection Status Changed to:', newVal)
    
            setSocket({ 'socketConnectedState': newVal })
        })
      }, 5000);
     
    clearInterval(interval);

    uibuilder.onChange('msg', function(msg){
        console.info('[uibuilder.onChange] property msg changed!', msg)
        setNodeMsgRecv(msg);
    })

    function sendMessage(topic, payload) {
        console.info('Sending a message to Node-red')
        let msg = uibuilder.send({'topic':topic,'payload':payload})
        setNodeMsgSent(msg);
    }
    
    function switchOnChange(){
        setLock(!lock)
    }

    const doCaptchaSubmit=(event)=>{
        if (validateCaptcha(user_captcha_value, false)===true) {
            setCaptchaDone(true);
            setCaptchaError("");
        }
        else {
            loadCaptchaEnginge(6);
            setCaptchaError("Error: Wrong Captcha try again")
            setCaptchaDone(false);
        }
        event.preventDefault();
    };

    const handleEmailSubmit = (event)=>{

        if(email.length > 5 && email.includes("@") && email.includes(".")){
            setEmailSubmitted(true);
            let msgcode= (Math.floor(100000 + Math.random() * 900000)).toString();
            setCode(msgcode);
            console.log(code);
            let msg = email+","+msgcode
            console.log(msg)
            sendMessage("verification",msg)
        }
        else{
            setEmailError("Wrong email format");
        }
        event.preventDefault();
    }

    const handleCode= (event) =>{
        
        if(userCode === code){
            sendMessage("beltControl", "open")
            event.preventDefault();
        }
        else{
            if ((numberOfTries - codeTries) < 0){
                sendMessage("beltControl","wrong code");
                setCaptchaDone(true)
                setEmailSubmitted(false)
                setLock(false)
                setCodeTries(0)
                props.setUser(undefined)
                navigate("/chindogu/login");
            }else{
                setCodeError("Wrong code, number of tries left:"+(numberOfTries - codeTries).toString());
                sendMessage("beltControl","wrong code");
                setCodeTries(codeTries+1);
                event.preventDefault();
            }
        }
      //  event.preventDefault();
    }

    function handleLogout(){
        props.setUser(undefined)
        navigate("/chindogu/login");
        
    }

    function validateEmail() {
        return email.length > 5 && email.includes("@") && email.includes(".");
    }
    function validateCode(){
        return userCode.length === 6 && /^\d+$/.test(userCode);
    }

    function renderSwitch (){
        return (<div> 
                    <h2>Toggle belt lock:
                    <label class="switch">
                    <input type="checkbox" onChange={switchOnChange}/>
                    <span class="slider"></span>
                    </label></h2>
                </div>);
    }

    function renderCaptcha(){ 
        return( <div>
                    <h5>Please complete captcha to prove humanity:</h5>
                    <LoadCanvasTemplateNoReload />
                    <input placeholder="Enter Captcha Value" value={user_captcha_value} id="user_captcha_input" name="user_captcha_input" type="text" onChange={(e) => setUserCaptcha(e.target.value)} ></input>
                    <button  onClick={doCaptchaSubmit}>Submit</button>
                    {captchaError===("") ?("") :(<p>{" "}{captchaError}</p>)}
                </div>)
    }


    function renderEmail(){
        return(
            <div>
                <Form onSubmit={handleEmailSubmit}>
                    <Form.Group controlId="Email">
                        <Form.Label>Email:</Form.Label>
                        <Form.Control
                            autoFocus
                            type="Email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}/>
                        {emailError===("") ?(""): (<p>{" "}{emailError}</p>) }
                    </Form.Group>
                    <Button  size="lg" type="submit" disabled={!validateEmail()}>
                    Submit
                    </Button>
                </Form>

            </div>)
    }

    function renderCodeSubmit(){
        return(
            <div>
                <Form onSubmit={handleCode}>
                    <Form.Group controlId="Code">
                    <Form.Label>Code:</Form.Label>
                    <Form.Control
                        autoFocus
                        type="Code"
                        value={userCode}
                        onChange={(e) => setUserCode(e.target.value)}/>
                    {codeError===("") ?(""): (<p>{" "}{codeError}</p>) }
                    </Form.Group>
                    <Button  size="lg" type="submit" disabled={!validateCode()}>
                    Submit
                    </Button>
                </Form>
            </div>)
    }

    function renderNodeBtn(){
        return (
            <div>
                <button onClick={() => setNoderedToggle(!noderedToggle)} >
                    Toogle Node-red message info
                </button>
            </div>);
    }

    function renderNoderedInfo(){
		return(
            <div>    
                <div >
                    <div>Last msg Received:</div>
                    <pre><code>{JSON.stringify(nodeMsgRecv, null, 2)}</code></pre>
                </div>

                <div >
                    <div>last Msg Sent</div>
                    <pre><code>{JSON.stringify(nodeMsgSent, null, 2)}</code></pre>
                </div>
                <div>Socket Connected?: {socket}</div>
            </div>
		);
	}

    return(
    <div className='App'> 
        <div className='App-header' id='header'>
            <h1>Chindogu Belt Dashboard of { props.user===undefined ? ("") : props.user.username } </h1>
        </div>
        <b/>
        <h2>Current belt status:{beltStatus===("") ?(""): (<h2>{" "}{beltStatus}</h2>) }</h2>{captchaDone===false ? (""): renderSwitch()}
        {captchaDone===true ? (""): renderCaptcha()}
        <b/>
        { lock === true && emailSubmitted === false ? renderEmail() : ("") }
        { lock === true && emailSubmitted === true ? renderCodeSubmit() : ("") }
        <b/>
        
        {captchaDone === true ? renderNodeBtn() : ("")}
        {noderedToggle === true ? renderNoderedInfo()   : ("")}  
        <button onClick={handleLogout} >Logout</button>
    </div>
   
    );  
}


export default Dashboard 