
import {React, useState, useCallback} from "react";
import Form from "react-bootstrap/Form";
import {User} from './App';
import {useNavigate} from 'react-router-dom';


function Login(props){

    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("");
    const [errorMessage, setErrorMessages] = useState(undefined);
    const navigate = useNavigate();
    const navigateCreate  = useCallback(() => navigate('chindogu/create'), [navigate]);
   // const updateUser = useCallback((u) => props.setUser(u))
   
      const errors = {
        uname: "invalid username",
        pass: "invalid password"
      };


    function validateForm() {
        return username.length > 0 && password.length > 0;
      }

    function wtf(){
      navigate('/chindogu/create');
    }
    
    const handleSubmit=(event) => {
       

        const userData = props.database.find((entry) => entry.username === username);
        if (userData) {
            if (userData.password !== password) {
              // Invalid password
              setErrorMessages({ name: "pass", message: errors.pass});
              event.preventDefault();
            } else {
                const user = new User(userData.username, userData.password);
               props.setUser(user);
                setErrorMessages(undefined)
              //  event.preventDefault();
                navigate("/chindogu/dashboard");
            }
          } else {
            // Username not found
            setErrorMessages({ name: "uname", message: errors.uname });
            event.preventDefault();
          }
          
    }

    return(
        <div className="App" >
            <div className="App-header"><h1>Welcome to the Smart Chindogu Belt </h1></div>
        
        <b/>
        <h3>Login:</h3>
      
      <Form onSubmit={handleSubmit}>
      <div className="myInput">
        <Form.Group controlId="Username">
          <Form.Label>Username:</Form.Label>
          <Form.Control
            autoFocus
            type="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="Password">
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          
           {errorMessage ? (<p>{" "}{errorMessage.name}: {errorMessage.message}</p>) : ("")}
        </Form.Group>
        </div>

          <button  size="lg" type="submit" disabled={!validateForm()}>
            Login
          </button>
      </Form>
      <button  size="lg" type="create" onClick={ wtf} >
            Create User
          </button>
        </div>
    );
}

export default Login